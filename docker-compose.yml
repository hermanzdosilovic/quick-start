version: "3.8"

x-common: &common
  logging:
    driver: json-file
    options:
      max-size: 100M
  restart: always

services:
  traefik:
    <<: *common
    image: traefik:v2.10.7 # https://hub.docker.com/_/traefik/tags?page=1&ordering=last_updated
    volumes:
      - ./config/traefik.yml:/etc/traefik/traefik.yml:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
    ports:
      - "80:80"

  edgar_app:
    <<: *common
    image: registry.gitlab.com/edgar-group/edgar/webapp:latest
    deploy:
      replicas: 2
    volumes:
      - ./config/app-config.js:/usr/app/config/config.js:ro
    labels:
      - "traefik.http.routers.edgar_app.entrypoints=http"
      - "traefik.http.routers.edgar_app.rule=HostRegexp(`{any:.+}`)"

  edgar_postman:
    <<: *common
    image: registry.gitlab.com/edgar-group/edgar/webapp:latest
    command: ["pm2", "start", "--no-daemon", "config/server_scripts_and_configs/production_postman.json"]
    volumes:
      - ./config/app-config.js:/usr/app/config/config.js:ro

  edgar_code_runner:
    <<: *common
    image: registry.gitlab.com/edgar-group/code-runner:latest
    volumes:
      - ./config/code-runner-config.js:/usr/app/config/config.js:ro
    labels:
      - "traefik.http.routers.edgar_code_runner.entrypoints=http"
      - "traefik.http.routers.edgar_code_runner.rule=HostRegexp(`{any:.+}`) && PathPrefix(`/code-runner`)"
      - "traefik.http.routers.edgar_code_runner.middlewares=edgar_code_runner"
      - "traefik.http.middlewares.edgar_code_runner.stripprefix.prefixes=/code-runner"

  edgar_pg_runner_1:
    <<: *common
    image: registry.gitlab.com/edgar-group/pg-runner/pg-runner:latest
    deploy:
      replicas: 1
    volumes:
      - ./config/pg-runner-config-1.js:/usr/app/config/config.js:ro

  edgar_pg_runner_2:
    <<: *common
    image: registry.gitlab.com/edgar-group/pg-runner/pg-runner:latest
    deploy:
      replicas: 2
    volumes:
      - ./config/pg-runner-config-2.js:/usr/app/config/config.js:ro

  edgar_pg_runner_postgres:
    <<: *common
    image: registry.gitlab.com/edgar-group/pg-runner/postgres:latest
    environment:
      POSTGRES_PASSWORD: ChangeThisPassword!pg_runner_postgres
    volumes:
      - edgar_pg_runner_postgres_data:/var/lib/postgresql/data

  edgar_minio:
    <<: *common
    image: minio/minio:RELEASE.2024-01-01T16-36-33Z # https://hub.docker.com/r/minio/minio/tags?page=1&ordering=last_updated
    hostname: minio.edgar.local
    entrypoint: sh
    command: -c 'mkdir -p /data/{download,images,questionattachments,tmpupload} && minio server /data --address ":9000" --console-address ":9001"'
    environment:
      MINIO_ROOT_PASSWORD: ChangeThisPassword!minio
      MINIO_ROOT_USER: edgar
    volumes:
      - edgar_minio_data:/data

  edgar_postgres:
    <<: *common
    image: registry.gitlab.com/edgar-group/edgar/postgres:latest
    environment:
      POSTGRES_PASSWORD: ChangeThisPassword!postgres
    volumes:
      - edgar_postgres_data:/var/lib/postgresql/data

  edgar_mongo:
    <<: *common
    image: mongo:7.0.4 # https://hub.docker.com/_/mongo/tags?page=1&ordering=last_updated
    volumes:
      - edgar_mongo_data:/data/db

  judge0_ce_server:
    <<: *common
    image: judge0/judge0:1.13.0
    env_file: ./config/judge0.conf
    privileged: true

  judge0_ce_worker:
    <<: *common
    image: judge0/judge0:1.13.0
    command: ["./scripts/workers"]
    env_file: ./config/judge0.conf
    privileged: true

  judge0_postgres:
    <<: *common
    image: postgres:16.1 # https://hub.docker.com/_/postgres/tags?page=1&ordering=last_updated
    env_file: ./config/judge0.conf
    volumes:
      - judge0_postgres_data:/var/lib/postgresql/data

  judge0_redis:
    <<: *common
    image: redis:6.2.14 # https://hub.docker.com/_/redis?tab=tags&page=1&ordering=last_updated
    command: [
      "bash", "-c",
      'docker-entrypoint.sh --appendonly yes --requirepass "$$REDIS_PASSWORD"'
    ]
    env_file: ./config/judge0.conf
    volumes:
      - judge0_redis_data:/data

volumes:
  edgar_minio_data:
  edgar_mongo_data:
  edgar_pg_runner_postgres_data:
  edgar_postgres_data:
  judge0_postgres_data:
  judge0_redis_data:
