# Edgar - Quick Start

Ready-to-use scripts and configuration files to run [Edgar](https://ieeexplore.ieee.org/abstract/document/9079865) locally and in a production environment.

## Table of Contents

1. [About](#1-about)
2. [Prerequisites](#2-prerequisites)
    1. [Production Environment](#21-production-environment)
    2. [Local And Production Environment](#22-local-and-production-environment)
3. [Get Started](#2-get-started)
4. [How-Tos](#3-how-tos)
    1. [Adding Teachers](#31-adding-teachers)
    2. [Adding Students](#32-adding-students)
    3. [Delete Default User](#33-delete-default-user)
    4. [Using External Judge0 Host](#34-using-external-judge0-host)

## 1 About

After following these instructions, you will learn the following:

1. How to run Edgar locally with it being available at `http://localhost`.
2. How to run Edgar on your server in a production environment with it being available at `http://edgar.example.com`.

## 2 Prerequisites

### 2.1 Production Environment

1. You will need a server with a public IP address.
2. Update your DNS settings to create `A` record for `edgar.example.com`.
    - If you use Cloudflare, you can [proxy this DNS record through Cloudflare](https://developers.cloudflare.com/dns/manage-dns-records/reference/proxied-dns-records/) and get a free SSL certificate; thus, access Edgar via `https://edgar.example.com`.

### 2.2 Local And Production Environment

1. Install [Docker](https://docs.docker.com/get-docker).
2. Install [Docker Compose](https://docs.docker.com/compose/install).
3. (_Only for Ubuntu 22.04 and above_) Update `/etc/default/grub` to use Cgroup v1 (required by [Judge0](https://judge0.com)).
    ```bash
    sudo sed -i.bak 's/GRUB_CMDLINE_LINUX="\(.*\)"/GRUB_CMDLINE_LINUX="\1 systemd.unified_cgroup_hierarchy=0"/' /etc/default/grub
    sudo update-grub
    sudo reboot
    ```
4. (_Only for macOS and Windows_) Configure Docker Engine to use Cgroup v1. Find more information [here](https://stackoverflow.com/a/74348261).
5. Ensure you have credentials for some SMTP server that allows you to send emails programmatically.
    - You can use a freemium SMTP service like [SMTP2GO](https://smtp2go.com).
6. Ensure that no process uses port `80`.

## 2 Get Started

1. Clone this repository.
    ```bash
    git clone https://gitlab.com/edgar-group/quick-start.git edgar
    ```
2. (_Optional, but recommended for production_) Update passwords.
    1. Use [this link](https://www.random.org/passwords/?num=20&len=32&format=plain&rnd=new) to get random passwords that you will use later.
    2. Search for the following passwords and replace them with the previously randomly generated ones:
        - `ChangeThisPassword!pg_runner_postgres`
            - Located in files: [`docker-compose.yml`](docker-compose.yml), [`config/pg-runner-config-2.js`](config/pg-runner-config-2.js)
        - `ChangeThisPassword!minio`
            - Located in files: [`docker-compose.yml`](docker-compose.yml), [`config/app-config.js`](config/app-config.js)
        - `ChangeThisPassword!postgres`
            - Located in files: [`docker-compose.yml`](docker-compose.yml), [`config/app-config.js`](config/app-config.js), [`config/pg-runner-config-1.js`](config/pg-runner-config-1.js)
        - `ChangeThisSecret!SECRET`
            - Located in files: [`config/app-config.js`](config/app-config.js)
        - `ChangeThisSecret!hashEncryptionSecret`
            - Located in files: [`config/app-config.js`](config/app-config.js)
        - `ChangeThisPassword!judge0_redis`
            - Located in files: [`config/judge0.conf`](config/judge0.conf)
        - `ChangeThisPassword!judge0_postgres`
            - Located in files: [`config/judge0.conf`](config/judge0.conf)
3. Update SMTP credentials in [`config/app-config.js`](config/app-config.js).
4. (_Only for HTTPS_) If you will access Edgar via `https://edgar.example.com` set `proto` to `https` in [`config/app-config.js`](config/app-config.js).
5. Run the [`bin/clean_install.sh`](bin/clean_install.sh) script that will do the initial setup for you:
    ```bash
    ./bin/clean_install.sh
    ```
    On Windows, run:
    ```
    bin\clean_install.bat
    ```
    Note for Windows users: before running clean_install.bat make sure that git clone did not change LF to CRLF in `config/judge0.conf` file (this depends on your local git settings, see `autocrlf` setting in git). This file should not contain `\r`, only LF should be used.
6. Visit http://localhost for the local environment or `http(s)://edgar.example.com` for the production environment and use the following login credentials:
    - Username: `edgar`
    - Password: `edgar`
7. That is it; you are done!
8. (_Optional, but recommended for production_) Secure your Edgar instance by doing the following:
    - [Create yourself a new teacher account](#31-adding-teachers).
    - [Delete default `edgar` user.](#33-delete-default-user)

## 3 How-Tos

### 3.1 Adding Teachers

![](.gitlab/AddingTeachers.png)

1. Log in as a teacher.
2. Go to Administration -> Users (list).
3. Fill the form with the following values:
    - `First name` - their first name
    - `Last name` - their last name
    - `Email` - their email
    - `Username` - make up their username
    - `Alt. id` - repeat the username
    - `Alt. id2` - repeat the username
    - `Provider` - set to the value `local`
4. Click the "Add" button.
5. Add them to a course by going to Administration -> Teachers (list).
6. Choose them from a dropdown menu and click the "Add" button.
7. Their default password is `edgar`, so you need to change that by running the following script:
    ```bash
    ./bin/administration/update_user_password.sh [username] [password]
    ```
8. Make sure you can log in as them with the updated password.
9. Send them their username and password.
10. That is it; you are done!

### 3.2 Adding Students

![](.gitlab/AddingStudents.png)

1. Log in as a teacher.
2. Go to Administration -> Upload students.
3. You are presented with the tab-separated, meaning each column is separated with one TAB. If you want to skip a column, press TAB.
4. Treat `alt_id` and `alt_id2` as usernames while `group_name` you can skip. Value for `auth_provider` should be `local`.
5. Once you add the students, click the "Parse" button.
6. Confirm that everything is parsed as expected, then click the "Insert new" button.
7. You will then be redirected to the Administration -> Students (list).
8. Click on the "Send register emails" button.
9. That is it; you are done!

### 3.3 Delete Default User

1. Run the following script to delete the default `edgar` user:

```bash
./bin/administration/delete_edgar_user.sh
```

### 3.4 Using External Judge0 Host

Out of the box, this repository includes Judge0, which is run alongside Edgar. But for your Edgar instance to work correctly, you don't need to run Judge0 yourself; you can connect Edgar to an external Judge0 endpoint, and in this example, we will use the [official Judge0 Shared Cloud](https://judge0.com/ce).

Please note that the following instructions only work when your Edgar instance is publicly available, i.e., in the production environment described above:

1. In file [`docker-compose.yml`](docker-compose.yml) delete all services whose names start with `judge0_`.
2. In file [`docker-compose.yml`](docker-compose.yml) delete all volumes whose names start with `judge0_`.
3. Visit https://judge0.com/ce and get your free API key for Judge0 CE on Judge0 Shared Cloud.
4. Update file [`config/code-runner-config.js`](config/code-runner-config.js) as follows:
    1. Update the `host` in `WORKER_HOSTS` to `https://judge0-ce.p.rapidapi.com`.
    2. Update the `CALLBACK_BASE_URL` to `http://edgar.example.com/code-runner` or `https://edgar.example.com/code-runner`. Make sure to use the correct protocol.
    3. Update the `HEADERS` as follows:
        ```javascript
        HEADERS: {
            "X-RapidAPI-Key": "xxxxxxxxxxxxxx"
        }
        ```
    4. Restart all services.
        ```bash
        docker compose up -d --force-recreate --remove-orphans
        ```
