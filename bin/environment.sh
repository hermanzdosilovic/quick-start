#!/usr/bin/env bash
DOCKER_COMPOSE="docker compose"
if ! command -v $DOCKER_COMPOSE &> /dev/null; then
    DOCKER_COMPOSE="docker-compose"
fi

export DOCKER_COMPOSE
