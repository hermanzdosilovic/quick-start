#!/usr/bin/env bash
set -e

source "./bin/environment.sh"

$DOCKER_COMPOSE down -v --remove-orphans
$DOCKER_COMPOSE up -d --pull always --remove-orphans edgar_postgres edgar_pg_runner_postgres
sleep 10s
$DOCKER_COMPOSE up -d --remove-orphans
