#!/usr/bin/env bash
source "./bin/environment.sh"

cat << EOF | $DOCKER_COMPOSE exec -T edgar_postgres psql -U postgres -d edgar
DELETE FROM student_course WHERE id_student = (SELECT id FROM student WHERE id_app_user = 1);
DELETE FROM local_provider WHERE id_student = (SELECT id FROM student WHERE id_app_user = 1);
DELETE FROM app_user_role WHERE id_app_user = 1;
DELETE FROM perm_user_course WHERE id_user = 1;
DELETE FROM student WHERE id_app_user = 1;
DELETE FROM app_user WHERE id = 1;
EOF
