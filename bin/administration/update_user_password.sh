#!/usr/bin/env bash
username="$1"
password="$2"

if [[ "$username" == "" || "$password" == "" ]]; then
    echo "Usage: $0 <username> <password>"
    exit 1
fi

hashed_password="$(htpasswd -nbBC 9 "$username" "$password" | cut -d ':' -f 2)" # sudo apt install apache2-utils

source "./bin/environment.sh"

cat << EOF | $DOCKER_COMPOSE exec -T edgar_postgres psql -U postgres -d edgar
UPDATE local_provider SET password = '$hashed_password' WHERE username = '$username';
EOF
