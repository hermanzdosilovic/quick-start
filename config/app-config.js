module.exports = {
  PG_CONNECTION_STRING: 'pg://postgres:ChangeThisPassword!postgres@edgar_postgres:5432/edgar',
  SECRET: 'ChangeThisSecret!SECRET',
  db: 'edgar',
  mongooseConnectionString: 'mongodb://edgar_mongo:27017/edgar',
  mail: {
    intervalSeconds: 30,
    waitBetweenMailsMinMillis: 500,
    waitBetweenMailsMaxMillis: 2500,
    batchSize: 50,
    host: 'smtp.example.com',
    secureConnection: false,
    port: 587,
    user: 'noreply@example.com',
    from: 'Edgar <noreply@example.com>',
    password: 'xxxxxxxxxxxxxx',
    tls: {
      ciphers: 'SSLv3',
    },
  },
  useMinIO: true,
  minioConfig: {
    endPoint: 'minio.edgar.local',
    port: 9000,
    useSSL: false,
    accessKey: 'edgar',
    secretKey: 'ChangeThisPassword!minio',
  },
  providers: {
    aai: false,
    dummy: false,
    facebook: false,
    google: false,
    local: true,
    twitter: false,
  },
  register: {
    hashSaltRounds: 9,
    hashEncryptionSecret: 'ChangeThisSecret!hashEncryptionSecret',
    tokenExpiry: 1000 * 2 * 7 * 24 * 3600,
  },
  tmpPath: '/tmp',
  proto: 'http'
};
