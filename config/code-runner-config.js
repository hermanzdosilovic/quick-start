module.exports = {
    WORKER_HOSTS: [
        {
            host: "http://judge0_ce_server:2358",
            weight: 1,
        }
    ],
    WORKER_PATH: "/submissions/batch",
    HTTP_GET_TOKEN_TIMEOUT: 5000,
    HTTP_GET_RESULT_TIMEOUT: 20000,
    BASE64_ENCODED: true,
    WAIT: false,
    CALLBACK_BASE_URL: "http://edgar_code_runner:10084",
    WAITING_ROOM_TIMEOUT: 5000,
    SUBMISSION_QUEUE_TIMEOUT: 60000,
    MAX_ACTIVE_SUBMISSIONS: 10000,
    REPORT_STATS_EVERY_MINUTES: 5,
    HEADERS: {}
};
