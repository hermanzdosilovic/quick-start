module.exports = {
    PUBLIC: 1,
    TIMEOUT: 15000,
    MAX_INSTANCES: 1,
    URL: '/run',
    DB: 'examdb',
    poolConfig: {
        user: 'postgres',
        database: 'examdb',
        password: 'ChangeThisPassword!pg_runner_postgres',
        port: 5432,
        host: 'edgar_pg_runner_postgres',
        max: 100,
        idleTimeoutMillis: 30000,
        connectionTimeoutMillis: 2000,
    }
};
